FROM ubuntu:18.04

MAINTAINER LINDAT/CLARIN <lindat-technical@ufal.mff.cuni.cz>

ENV DEBIAN_FRONTEND=noninteractive
ADD packages /opt/packages
WORKDIR /opt/kontext/

RUN apt-get update -q && \
    apt-get install -q -y git build-essential curl tcl \
        libxml2-dev libxml2-utils libxslt-dev libpcre3-dev libicu-dev zlib1g-dev libltdl-dev \
        python python-dev python-pip python-pyicu \
        gunicorn language-pack-en

RUN curl -O http://download.redis.io/redis-stable.tar.gz && \
    tar xzvf redis-stable.tar.gz && cd redis-stable && \
    make && make install

RUN cd /opt/packages && \
    dpkg -i manatee-open_2.158.8-1ubuntu1_amd64.deb && \
    dpkg -i python-signalfd_0.1-1ubuntu1_amd64.deb && \
    dpkg -i manatee-open-python_2.158.8-1ubuntu1_amd64.deb

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs

ENV PM2_HOME=/opt/pm2
RUN npm install -g pm2

ADD run /opt/run
RUN cd /opt/run && \
    chmod +x /opt/run/*.sh && \
    pip install -U --ignore-installed -r requirements.txt

WORKDIR /opt/run/
CMD [ "./start.sh" ]
