#!/bin/bash
KONTEXTDIR=/opt/kontext/installation
if [[ ! -d ${KONTEXTDIR} ]]; then
    echo "mount kontext to $KONTEXTDIR e.g., -v /path/to/kontext:$KONTEXTDIR"
    exit 1
fi

echo "Starting REDIS"
pm2 start redis-server --interpreter=none --name "kontext-redis" -- /opt/run/redis.conf

cd $KONTEXTDIR

echo "Installing npm install"
time npm install
echo "Installing make production"
time make production


echo "Running kontext - not returning"
PORT=5000
python public/app.py --address 0.0.0.0 --port ${PORT}
