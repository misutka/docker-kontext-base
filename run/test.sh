#!/bin/bash
set -e -o pipefail

echo "Testing python manatee import"
python -c "import manatee; dir(manatee); print; print manatee.version()"
